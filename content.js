const ACTION_KEY_CODE = 91 // 91 being mac's CMD button
const KEY_TARGET = 'data-target'

const id = 'crumbselectorextcontent'

let visible = false

const containerStyle = `
  width: 100%;
  position: fixed;
  top: 8px;
  display: flex;
  justify-content: center;
  z-index: 9999999 !important;
  box-sizing: border-box;
  font-size: 16sp;
  font-family: "Roboto", sans-serif
`
const linkButtonStyle = `
  background: white;
  margin-right: 0.1em;
  padding: 3px;
  border: 1px solid grey;
  border-radius: 0.3em;
  color: black;
`

function createLinkButton(target, text) {
  var button = document.createElement('button')
  button.className += 'crumbselector-link-button'
  button.style = linkButtonStyle

  button.setAttribute(KEY_TARGET, target)
  button.innerText = text

  button.addEventListener('click', event => {
    event.preventDefault()

    window.location.href = '' + event.target.getAttribute(KEY_TARGET)
  })

  return button
}

function show() {
  var container = document.createElement('div')
  container.id = id
  container.style = containerStyle

  const url = window.location.href.split('/')
  const protocol = url.splice(0, 2)[0]

  if (url[url.length - 1].length === 0) url.splice(url.length - 1, 1)
  url.splice(url.length - 1, 1) // Remove current page

  let builder = protocol + '//'

  for (index in url) {
    builder += url[index] + '/'

    const button = createLinkButton(builder, url[index])

    container.append(button)
  }

  const body = document.querySelector('body')
  body.insertBefore(container, body.firstChild)

  visible = true
}

function hide() {
  var container = document.querySelector('#' + id)

  if (container) {
    container.parentElement.removeChild(container)

    visible = false
  }
}

const active_keys = []
document.addEventListener('keydown', event => {
  active_keys.push(event.keyCode)

  if (event.keyCode === ACTION_KEY_CODE && active_keys.length === 1)
    show()
  else if (visible) hide()
})

document.addEventListener('keyup', event => {
  const index = active_keys.indexOf(event.keyCode)
  active_keys.splice(index, 1)

  if (visible) hide()
})
document.addEventListener('visibilitychange', event => {
  active_keys.length = 0

  if (visible) hide()
})

const styling = `
.crumbselector-link-button:hover {
  border: 2px solid black;
  padding: 2px;
}
`

const head = document.querySelector('head')
const style = document.createElement('style')

if (style.styleSheet) style.styleSheet.cssText = styling
else style.appendChild(document.createTextNode(styling))

head.appendChild(style)
