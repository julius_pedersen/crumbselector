# Crumbselector Chrome extension
## What
Splits the URL into clickable elements to faster navigate up the tree

## Why
Jesper wanted it

## How

1. ```git clone git clone git@bitbucket.org:julius_pedersen/crumbselector.git```
2. In Chrome, go to [chrome://extensions/](chrome://extensions/) and enable
developer mode. 
3. Click load unpacked and navigate to the cloned directory. Open it.
